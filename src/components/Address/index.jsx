import { ETHERSCAN_DOMAIN } from "configs";
import React from "react";

function Address({ wallet_address, children }) {
  const getUrl = () => {
    return new URL(`address/${wallet_address}`, ETHERSCAN_DOMAIN).href;
  };
  return (
    <>
      <a href={getUrl()} target="_blank" rel="noopener noreferrer">
        {children || wallet_address}
      </a>
    </>
  );
}

export default Address;
