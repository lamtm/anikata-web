import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import { Layout as AntLayout } from "antd";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { Link, Outlet } from "react-router-dom";
import BreadCrumb from "../BreadCrumb";
import HeaderAction from "../HeaderAction";
import Logo from "../Icons/Logo";
import Menu from "../Menu";
import style from "./Layout.module.scss";

const { Header: AntHeader, Content: AntContent, Sider: AntSider } = AntLayout;

function Layout() {
  const [t] = useTranslation(["common"], { useSuspense: false });
  const [collapsed, setCollapsed] = useState(false);

  return (
    <AntLayout>
      <AntSider
        theme="light"
        trigger={React.createElement(
          collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
          {
            className: "trigger",
            onClick: () => setCollapsed(!collapsed),
          }
        )}
        collapsible
        collapsedWidth="48"
        collapsed={collapsed}
        width={260}
      >
        <Link className={style.sidebar_header} to="/arcana">
          <div className={style.logo}>
            <Logo />
            <h1 className={style.sidebar_title}>
              {t("anicana_title_sidebar")}
            </h1>
          </div>
        </Link>
        <Menu />
      </AntSider>
      <AntLayout className={style.site_layout}>
        <AntHeader className={style.site_layout_background}>
          <BreadCrumb />
          <HeaderAction />
        </AntHeader>
        <AntContent className={style.site_layout_background}>
          <Outlet />
        </AntContent>
      </AntLayout>
    </AntLayout>
  );
}

export default Layout;
