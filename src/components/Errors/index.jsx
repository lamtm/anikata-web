import React from "react";
import style from "./Errors.module.scss";
import { Alert } from "antd";

function Errors({ errors }) {
  return (
    <div className={style.container}>
      {errors?.map((error, index) => (
        <div className={style.error} key={index}>
          <Alert banner message={error} type="error" />
        </div>
      ))}
    </div>
  );
}

export default Errors;
