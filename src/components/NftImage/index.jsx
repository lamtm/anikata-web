import { IPFS_DOMAIN } from "configs";
import React from "react";

function NftImage({ className, image_uri, ...props }) {
  return (
    <>
      <img
        className={className}
        src={new URL(image_uri?.replace("ipfs://", ""), IPFS_DOMAIN).href}
        {...props}
      />
    </>
  );
}

export default NftImage;
