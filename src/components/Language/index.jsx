import React from "react";
import { useDispatch } from "react-redux";
import { Dropdown, Menu } from "antd";
import { DownOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";
import UkFlag from "../Icons/UkFlag";
import JapanFlag from "../Icons/JapanFlag";
import { setLang } from "../App/AppSlice";
import style from "./Language.module.scss";

export default function Language() {
  const { t, i18n } = useTranslation(["common"], { useSuspense: false });
  const dispatch = useDispatch();
  const setLanguage = (lng) => {
    i18n.changeLanguage(lng);
    dispatch(setLang(lng));
  };

  const menu = (
    <Menu>
      <Menu.Item
        icon={<JapanFlag width={24} />}
        key="jp"
        onClick={() => setLanguage("jp")}
      >
        {t("japanese")}
      </Menu.Item>
      <Menu.Item
        icon={<UkFlag width={24} />}
        key="en"
        onClick={() => setLanguage("en")}
      >
        {t("english")}
      </Menu.Item>
    </Menu>
  );

  return (
    <Dropdown
      className={style.language_choice}
      overlay={menu}
      placement="bottomLeft"
      trigger={["click"]}
    >
      <div>
        {i18n.language === "jp" ? (
          <>
            <JapanFlag width={24} /> {t("japanese")}
          </>
        ) : (
          <>
            <UkFlag width={24} /> {t("english")}
          </>
        )}
        <DownOutlined className="ms-3" />
      </div>
    </Dropdown>
  );
}
