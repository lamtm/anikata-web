import { Breadcrumb } from "antd";
import React from "react";
import { useLocation } from "react-router";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";

export default function BreadCrumb() {
  const { t } = useTranslation(["common"], { useSuspense: false });
  const location = useLocation();
  const breadcrumbNameMap = {
    "/arcana": t("arcana_breadcrumb"),
    "/egg": t("egg_breadcrumb"),
    "/shard": t("shard_breadcrumb"),
    "/matrix": t("matrix_breadcrumb"),
    "/404": t("not_found_breadcrumb"),
    home: t("home_breadcrumb"),
  };
  const pathSnippets = location.pathname.split("/").filter((i) => i);
  const extraBreadcrumbItems = pathSnippets.map((path, index) => {
    const url = `/${pathSnippets.slice(0, index + 1).join("/")}`;
    return (
      <Breadcrumb.Item key={url}>
        <Link to={url}>{breadcrumbNameMap[url] || path}</Link>
      </Breadcrumb.Item>
    );
  });
  const breadcrumbItems = [].concat(extraBreadcrumbItems);

  return (
    <div>
      {breadcrumbItems?.length === 0 ? (
        <Breadcrumb>
          <Breadcrumb.Item>
            <Link to="#">{breadcrumbNameMap.home}</Link>
          </Breadcrumb.Item>
        </Breadcrumb>
      ) : (
        <Breadcrumb>{breadcrumbItems}</Breadcrumb>
      )}
    </div>
  );
}
