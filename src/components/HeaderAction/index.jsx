import React from "react";
import Language from "../Language";

function HeaderAction() {
  return (
    <div>
      <Language />
    </div>
  );
}

export default HeaderAction;
