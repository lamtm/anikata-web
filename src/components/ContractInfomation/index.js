import { Button, Input } from "antd";
import Address from "components/Address";
import React, { useRef, useState } from "react";
import { useTranslation } from "react-i18next";
import style from "./ContractInfomation.module.scss";
import BorderColorIcon from "@mui/icons-material/BorderColor";
import { batch } from "react-redux";
import Errors from "components/Errors";
import { getContract, web3 } from "utils/web3";
import Web3 from "web3";

function ContractInfomation({ address, abi, total, setAddress, setContract }) {
  const [t] = useTranslation(["common"], { useSuspense: false });
  let inputRef = useRef();
  const [edit, setEdit] = useState(false);
  const [errors, setErrors] = useState();
  const [isDisabled, setIsDisabled] = useState(true);

  React.useEffect(() => {
    initContract(address);
  }, []);

  React.useEffect(() => {
    if (edit) {
      inputRef.current.focus();
    } else {
      setIsDisabled(true);
      setErrors([]);
    }
  }, [edit]);

  const getNftContract = async (address) => {
    const { web3Contract } = getContract(abi, address);
    if (await isContractAddress(address)) {
      setEdit(false);
      return web3Contract;
    } else {
      throw { message: `Not found contract address ${address}.` };
    }
    return null;
  };

  const isContractAddress = async (address) => {
    return "0x" != (await web3.eth.getCode(address));
  };

  const initContract = async (address) => {
    try {
      const newContract = await getNftContract(address);
      if (newContract) {
        setAddress(address.toString());
        setContract(newContract);
      }
    } catch (error) {
      setErrors([error.message]);
    }
  };

  const handleKeyDown = async (e) => {
    setIsDisabled(false);
    if (e.key == "Enter") {
      const address = inputRef.current?.input?.value;
      initContract(address);
    }
  };

  return (
    <>
      <div className={style.contract_info}>
        <div className={style.address_container}>
          <span>{t("contract")}:</span>&nbsp;
          <div className={style.value}>
            {edit ? (
              <>
                <Input
                  ref={inputRef}
                  name="address"
                  bordered={false}
                  placeholder="Enter contract address..."
                  autoComplete="off"
                  defaultValue={address}
                  onKeyDown={handleKeyDown}
                />
                <Button
                  onClick={() => initContract(inputRef.current?.input?.value)}
                  disabled={isDisabled}
                >
                  OK
                </Button>
                <Button onClick={() => setEdit(false)}>Cancel</Button>
              </>
            ) : (
              <>
                <Address wallet_address={address} />
                <BorderColorIcon
                  style={{ fontSize: 16, cursor: "pointer" }}
                  color="action"
                  onClick={() => setEdit(true)}
                />
              </>
            )}
          </div>
        </div>
        {(!errors || errors.length < 1) && (
          <div className={style.total}>
            <span>{t("total")}:</span>&nbsp;
            <span>{total}</span>
          </div>
        )}
      </div>

      <Errors errors={errors} />
    </>
  );
}

export default ContractInfomation;
