import DiamondIcon from "@mui/icons-material/Diamond";
import EggOutlinedIcon from "@mui/icons-material/EggOutlined";
import PentagonIcon from "@mui/icons-material/Pentagon";
import SpokeIcon from "@mui/icons-material/Spoke";
import { Menu as AntMenu } from "antd";
import { Link, useLocation } from "react-router-dom";
import style from "./Menu.module.scss";

const menuItems = [
  {
    key: "arcana",
    icon: <DiamondIcon style={{ fontSize: 22 }} />,
    label: (
      <Link to={"/arcana"}>
        <span className={`ms-4 ${style.menu_item_text}`}>ARCANA</span>
      </Link>
    ),
  },
  {
    key: "egg",
    icon: <EggOutlinedIcon style={{ fontSize: 22 }} />,
    label: (
      <Link to={"/egg"}>
        <span className={`ms-4 ${style.menu_item_text}`}>EGG</span>
      </Link>
    ),
  },
  {
    key: "shard",
    icon: <SpokeIcon style={{ fontSize: 22 }} />,
    label: (
      <Link to={"/shard"}>
        <span className={`ms-4 ${style.menu_item_text}`}>SHARD</span>
      </Link>
    ),
  },
  {
    key: "matrix",
    icon: <PentagonIcon style={{ fontSize: 22 }} />,
    label: (
      <Link to={"/matrix"}>
        <span className={`ms-4 ${style.menu_item_text}`}>MATRIX</span>
      </Link>
    ),
  },
];

function Menu() {
  const location = useLocation();
  const getSelectedKey = () => [location.pathname.split("/")[1]];

  return (
    <AntMenu
      style={{ width: 256 }}
      mode="inline"
      items={menuItems}
      selectedKeys={getSelectedKey()}
    />
  );
}

export default Menu;
