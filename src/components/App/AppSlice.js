import { createSlice } from "@reduxjs/toolkit";

const lang = localStorage.getItem("i18nextLng");
const initialState = {
  lang: lang || "jp",
  loading: false,
};

const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    setLang: (state, action) => {
      localStorage.setItem("i18nextLng", action.payload);
      state.lang = action.payload;
    },
    openLoading: (state) => {
      state.loading = true;
    },
    hideLoading: (state) => {
      state.loading = false;
    },
  },
});

export const { openLoading, hideLoading, setLang } = appSlice.actions;

export default appSlice.reducer;
