import React from "react";
import { Outlet } from "react-router-dom";
import "./App.scss";
import "../../i18n";

function App() {
  return (
    <div className="App">
      <Outlet />
    </div>
  );
}

export default App;
