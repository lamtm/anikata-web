import React from "react";
export default function JapanFlag(props) {
  return <img src="/assets/images/jp.png" {...props} alt="japan" />;
}
