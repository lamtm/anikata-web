import React from "react";
import PropTypes from "prop-types";
export default function Icons({ width, height, name, type, ...props }) {
  return (
    <img
      loading="lazy"
      src={`/images/icon/${name}.${type}`}
      alt={name}
      {...{ width, height }}
      {...props}
    />
  );
}
Icons.defaultProps = {
  type: "svg",
};
Icons.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  name: PropTypes.string.isRequired,
};
