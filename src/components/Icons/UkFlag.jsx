import React from "react";

export default function UkFlag(props) {
  return <img src="/assets/images/uk.png" {...props} alt="uk" />;
}
