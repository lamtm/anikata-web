import reactDom from "react-dom";
import { Spin } from "antd";
import React from "react";
import { useSelector } from "react-redux";

function LoadingSpinner() {
  return reactDom.createPortal(
    <div id="loading-spinner">
      <Spin spinning size="large" />
    </div>,
    document.getElementById("loading")
  );
}

export default function RootLoading() {
  const loading = useSelector((state) => state.app.loading);
  return <>{loading && <LoadingSpinner />}</>;
}
