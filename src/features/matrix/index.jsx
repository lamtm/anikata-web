import { Spin, Table } from "antd";
import { hideLoading, openLoading } from "components/App/AppSlice";
import ContractInfomation from "components/ContractInfomation";
import { nftMatrix } from "configs";
import { ITEMS_NAME } from "constants/index";
import { range } from "lodash";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { batch, useDispatch } from "react-redux";
import style from "./Matrix.module.scss";

const initDataList = {
  currentPage: 1,
  pageSize: 10,
  total: null,
  dataSource: [],
  errors: [],
};

function Matrix() {
  const dispatch = useDispatch();
  const [t] = useTranslation(["common"], { useSuspense: false });
  const [dataList, setDataList] = useState(initDataList);
  const [address, setAddress] = useState(
    localStorage.getItem(ITEMS_NAME.MATRIX_CONTRACT_ADDRESS) ||
      nftMatrix.address
  );
  const [nftMatrixContract, setNftMatrixContract] = useState();

  React.useEffect(() => {
    batch(async () => {
      localStorage.setItem(ITEMS_NAME.MATRIX_CONTRACT_ADDRESS, address);
      await dispatch(openLoading());
      await fetchListNFTs();
      await dispatch(hideLoading());
    });
  }, [nftMatrixContract]);

  const fetchListNFTs = async (currentPage) => {
    if (!nftMatrixContract) return;
    try {
      setDataList((prev) => ({ ...prev, loading: true }));
      const total = await nftMatrixContract.methods.numRegistered().call();

      if (+total < 1) {
        setDataList({ ...initDataList, loading: false });
      } else {
        getPriceList(currentPage ?? 1, dataList.pageSize, +total);
      }
    } catch (e) {
      setDataList({ ...initDataList, loading: false, errors: [e.message] });
    }
  };

  async function getPriceList(currentPage, pageSize, total) {
    const firstId = pageSize * (currentPage - 1) + 1;
    const lastId = Math.min(pageSize * currentPage, total);

    try {
      const data = await Promise.all(
        range(firstId, lastId + 1).map(async (tokenId) => {
          const price = await nftMatrixContract.methods
            .getPrice(tokenId)
            .call();
          return { tokenId, price, key: tokenId };
        })
      );

      setDataList((prev) => ({
        ...prev,
        dataSource: data,
        total: total,
        currentPage: currentPage ?? 1,
        loading: false,
        errors: [],
      }));
    } catch (e) {
      setDataList({ ...initDataList, loading: false, errors: [e.message] });
    }
  }

  const { currentPage, total, pageSize, dataSource, loading } = dataList;
  const columns = [
    {
      title: "matrixId",
      dataIndex: "tokenId",
    },
    {
      title: "price (ANM)",
      dataIndex: "price",
    },
  ];

  const handlePageChange = (page) => {
    fetchListNFTs(page);
  };

  return (
    <div className="matrix_container">
      <Spin spinning={loading || false} size="medium">
        <ContractInfomation
          address={address}
          abi={nftMatrix.abi}
          setContract={setNftMatrixContract}
          total={total}
          setAddress={setAddress}
        />
        <Table
          size="small"
          className={style.table}
          columns={columns}
          dataSource={dataSource}
          pagination={{
            defaultCurrent: 1,
            defaultPageSize: pageSize,
            position: ["bottomCenter"],
            showSizeChanger: false,
            total: dataSource?.length > 0 && total ? total : 0,
            onChange: handlePageChange,
            current: currentPage,
            hideOnSinglePage: true,
            responsive: true,
            showLessItems: true,
            className: style.paginator_custom,
          }}
          scroll={{ x: 638 }}
        />
      </Spin>
    </div>
  );
}

export default Matrix;
