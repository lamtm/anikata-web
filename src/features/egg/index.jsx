import { Button, Spin, Table } from "antd";
import React, { useEffect, useState } from "react";
import ArcanaDetail from "./EggDetail";
import style from "./Egg.module.scss";
import { useTranslation } from "react-i18next";
import { batch, useDispatch, useSelector } from "react-redux";
import { getContract } from "utils/web3";
import { range } from "lodash";
import { hideLoading, openLoading } from "components/App/AppSlice";
import { nftEggERC721 } from "configs";
import Address from "components/Address";
import ContractInfomation from "components/ContractInfomation";
import { ITEMS_NAME } from "constants/index";

const initDataList = {
  currentPage: 1,
  pageSize: 10,
  total: null,
  dataSource: [],
  errors: [],
};

function Egg() {
  const dispatch = useDispatch();
  const [t] = useTranslation(["common"], { useSuspense: false });
  const [modal, setModal] = useState({
    tokenId: null,
    tokenOwner: null,
    isModalVisible: false,
  });
  const [dataList, setDataList] = useState(initDataList);
  const [address, setAddress] = useState(
    localStorage.getItem(ITEMS_NAME.EGG_CONTRACT_ADDRESS) || nftEggERC721.address
  );
  const [nftEgg721Contract, setNftEgg721Contract] = useState();

  React.useEffect(() => {
    batch(async () => {
      localStorage.setItem(ITEMS_NAME.EGG_CONTRACT_ADDRESS, address);
      await dispatch(openLoading());
      await fetchListNFTs();
      await dispatch(hideLoading());
    });
  }, [nftEgg721Contract]);

  const fetchListNFTs = async (currentPage) => {
    if (!nftEgg721Contract) return;
    try {
      setDataList((prev) => ({ ...prev, loading: true }));
      const total = await nftEgg721Contract.methods.totalSupply().call();
      if (+total < 1) {
        setDataList({ ...initDataList, loading: false });
      } else {
        getOwnerList(currentPage ?? 1, dataList.pageSize, +total);
      }
    } catch (e) {
      console.error(e);
      setDataList({ ...initDataList, loading: false });
    }
  };

  async function getOwnerList(currentPage, pageSize, total) {
    const firstId = pageSize * (currentPage - 1) + 1;
    const lastId = Math.min(pageSize * currentPage, total);

    Promise.all(
      range(firstId, lastId + 1).map(async (tokenId) => {
        const rs = await nftEgg721Contract.methods
          .ownerOf(tokenId)
          .call()
          .then((tokenOwner) => {
            return { tokenId, tokenOwner, key: tokenId };
          });
        return rs;
      })
    )
      .then((data) => {
        setDataList((prev) => ({
          ...prev,
          dataSource: data,
          total: total,
          currentPage: currentPage ?? 1,
          loading: false,
          errors: [],
        }));
      })
      .catch((err) => {
        setDataList({ ...initDataList, loading: false, errors: [err.message] });
      });
  }

  const { currentPage, total, pageSize, dataSource, loading } = dataList;
  const columns = [
    {
      title: "tokenId",
      dataIndex: "tokenId",
    },
    {
      title: "tokenOwner",
      dataIndex: "tokenOwner",
      render: (tokenOwner) => <Address wallet_address={tokenOwner} />,
    },
    {
      title: "Action",
      dataIndex: "tokenId",
      render: (_, item) => (
        <Button onClick={() => showModal(item?.tokenId, item.tokenOwner)}>
          {t("details")}
        </Button>
      ),
    },
  ];

  const showModal = (tokenId, tokenOwner) => {
    setModal({ tokenId, tokenOwner, isModalVisible: true });
  };

  const handleCancel = () => {
    setModal({ tokenId: null, tokenOwner: null, isModalVisible: false });
  };

  const handlePageChange = (page) => {
    fetchListNFTs(page);
  };

  return (
    <div className="egg_container">
      <Spin spinning={loading || false} size="medium">
        <ContractInfomation
          address={address}
          abi={nftEggERC721.abi}
          setContract={setNftEgg721Contract}
          total={total}
          setAddress={setAddress}
        />
        <Table
          size="small"
          className={style.table}
          columns={columns}
          dataSource={dataSource}
          pagination={{
            defaultCurrent: 1,
            defaultPageSize: pageSize,
            position: ["bottomCenter"],
            showSizeChanger: false,
            total: dataSource?.length > 0 && total ? total : 0,
            onChange: handlePageChange,
            current: currentPage,
            hideOnSinglePage: true,
            responsive: true,
            showLessItems: true,
            className: style.paginator_custom,
          }}
          scroll={{ x: 638 }}
        />
      </Spin>
      {modal?.isModalVisible && modal?.tokenId && (
        <ArcanaDetail
          tokenId={modal?.tokenId}
          tokenOwner={modal?.tokenOwner}
          isModalVisible={modal?.isModalVisible}
          handleCancel={handleCancel}
        />
      )}
    </div>
  );
}

export default Egg;
