import { Button, Modal } from "antd";
import Address from "components/Address";
import { hideLoading, openLoading } from "components/App/AppSlice";
import NftImage from "components/NftImage";
import { nftEggERC721 } from "configs";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { batch, useDispatch } from "react-redux";
import { getContract } from "utils/web3";
import style from "./EggDetail.module.scss";

function ArcanaDetail({ tokenId, tokenOwner, handleCancel, ...props }) {
  const dispatch = useDispatch();
  const [t] = useTranslation(["common"], { useSuspense: false });
  const [details, setdetails] = useState({});

  const { web3Contract: nftEgg721Contract } = getContract(
    nftEggERC721.abi,
    nftEggERC721.address
  );

  React.useEffect(() => {
    batch(async () => {
      await dispatch(openLoading());
      await fetchDetails();
      await dispatch(hideLoading());
    });
  }, []);

  async function fetchDetails() {
    try {
      const [id, gene, metadataHash] = await nftEgg721Contract.methods
        .tokens(tokenId)
        .call();

      setdetails({
        tokenId: id,
        tokenOwner,
        gene,
        image: `ipfs://${metadataHash}`,
        metadataHash,
        loading: false,
      });
    } catch (e) {
      console.error(e);
      setdetails({ loading: false });
    }
  }

  return (
    <div className={style.modal_container}>
      <Modal
        footer={null}
        centered
        title="Arcana Detail"
        visible={details?.loading == false}
        onCancel={handleCancel}
        className={style.modal}
        {...props}
      >
        <div className={style.modal_content}>
          {/* <div className={style.token_img_container}>
            <NftImage
              className={style.token_img}
              image_uri={details?.image}
              alt="token-image"
            />
          </div> */}
          <div className={style.field}>
            <span className={style.label}>tokenId:</span>
            <span className={style.value}>{details?.tokenId}</span>
          </div>
          <div className={style.field}>
            <span className={style.label}>tokenOwner:</span>
            <span className={style.value}>
              <Address wallet_address={details?.tokenOwner} />
            </span>
          </div>
          <div className={style.field}>
            <span className={style.label}>gene:</span>
            <span className={style.value}>{details?.gene}</span>
          </div>
          <div className={style.field}>
            <span className={style.label}>metadata:</span>
            <span className={style.value}>{details?.metadataHash}</span>
          </div>
        </div>
        <div className={style.modal_action}>
          <Button onClick={handleCancel}>{t("close")}</Button>
        </div>
      </Modal>
    </div>
  );
}

export default ArcanaDetail;
