import { Spin, Table } from "antd";
import Address from "components/Address";
import { hideLoading, openLoading } from "components/App/AppSlice";
import { nftShardERC1155 } from "configs";
import { ITEMS_NAME } from "constants/index";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { batch, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { getContract } from "utils/web3";
import style from "../Shard.module.scss";

const initDataList = {
  currentPage: 1,
  pageSize: 10,
  totalSupply: null,
  totalOwner: null,
  dataSource: [],
  errors: [],
};

function ShardDetail() {
  const dispatch = useDispatch();
  const [t] = useTranslation(["common"], { useSuspense: false });
  const { tokenId } = useParams();
  const [dataList, setDataList] = useState(initDataList);
  const [nftShard1155Contract, setNftShard1155Contract] = useState();
  const [address] = useState(
    localStorage.getItem(ITEMS_NAME.SHARD_CONTRACT_ADDRESS) ||
      nftShardERC1155.address
  );

  React.useEffect(() => {
    try {
      const { web3Contract } = getContract(nftShardERC1155.abi, address);
      setNftShard1155Contract(web3Contract);
    } catch (error) {
      console.log("try catch: ", error);
    }
  }, [address]);

  React.useEffect(() => {
    batch(async () => {
      if (nftShard1155Contract) {
        await dispatch(openLoading());
        await fetchData();
        await dispatch(hideLoading());
      }
    });
  }, [nftShard1155Contract, tokenId]);

  async function fetchData(currentPage) {
    if (!nftShard1155Contract) return;
    try {
      setDataList((prev) => ({ ...prev, loading: true }));
      // get total supply
      const totalSupply = await getTotalSupplyOfToken(tokenId);
      if (+totalSupply < 1) {
        setDataList({ ...initDataList, totalSupply: 0, loading: false });
        return;
      }

      // get number of owner of current token
      const totalOwner = await getOwnersOfToken(tokenId);
      if (+totalSupply < 1) {
        setDataList({
          ...initDataList,
          totalSupply,
          totalOwner,
          loading: false,
        });
        return;
      }

      // get list owner & balance
      const ownerList = await getOwnerList(
        tokenId,
        currentPage ?? 1,
        dataList.pageSize
      );
      setDataList((prev) => ({
        ...prev,
        dataSource: ownerList,
        totalSupply,
        totalOwner,
        currentPage: currentPage ?? 1,
        loading: false,
        errors: [],
      }));
    } catch (e) {
      console.error(e);
      setDataList({ ...initDataList, loading: false });
    }
  }

  async function getTotalSupplyOfToken(tokenId) {
    const totalSupply = await nftShard1155Contract.methods
      .totalSupplyOfToken(tokenId)
      .call();
    return totalSupply;
  }

  async function getOwnersOfToken(tokenId) {
    const totalOwner = await nftShard1155Contract.methods
      .ownersOfToken(tokenId)
      .call();
    return totalOwner;
  }

  async function getOwnerList(tokenId, currentPage, pageSize) {
    const offset = (currentPage - 1) * pageSize;

    const rs = await nftShard1155Contract.methods
      .ownerOfTokenByIndexBatch(tokenId, offset, pageSize)
      .call();
    const ownerList = parseData(rs);
    return ownerList;
  }

  function parseData(rawData) {
    const data = rawData?.map((item) => {
      if (+item.balance > 0) {
        return {
          address: item.account,
          amount: item.balance,
          key: item.account,
        };
      }
    });
    return data;
  }

  const { currentPage, totalOwner, pageSize, dataSource, loading } = dataList;
  const columns = [
    {
      title: "Owner",
      dataIndex: "address",
    },
    {
      title: "Amount",
      dataIndex: "amount",
    },
  ];

  const handlePageChange = (page) => {
    fetchData(page);
  };

  return (
    <div className="container">
      <Spin spinning={loading || false} size="medium">
        <div style={{ padding: "0 0 20px 10px" }}>
          <p>
            Contract: <Address wallet_address={address} />
          </p>
          <p>Token ID: {tokenId}</p>
          <p>Total supply: {dataList.totalSupply}</p>
          <p>Holders: {totalOwner}</p>
        </div>

        <Table
          size="small"
          className={style.table}
          columns={columns}
          dataSource={dataSource}
          pagination={{
            defaultCurrent: 1,
            defaultPageSize: pageSize,
            position: ["bottomCenter"],
            showSizeChanger: false,
            total: dataSource?.length > 0 && totalOwner ? totalOwner : 0,
            onChange: handlePageChange,
            current: currentPage,
            hideOnSinglePage: true,
            responsive: true,
            showLessItems: true,
            className: style.paginator_custom,
          }}
          scroll={{ x: 638 }}
        />
      </Spin>
    </div>
  );
}

export default ShardDetail;
