import { Button, Spin, Table } from "antd";
import { hideLoading, openLoading } from "components/App/AppSlice";
import ContractInfomation from "components/ContractInfomation";
import { nftShardERC1155 } from "configs";
import { ITEMS_NAME } from "constants/index";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { batch, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import style from "./Shard.module.scss";

const initDataList = {
  currentPage: 1,
  pageSize: 10,
  total: null,
  dataSource: [],
  errors: [],
};

function Shard() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [t] = useTranslation(["common"], { useSuspense: false });
  const [dataList, setDataList] = useState(initDataList);
  const [address, setAddress] = useState(
    localStorage.getItem(ITEMS_NAME.SHARD_CONTRACT_ADDRESS) ||
      nftShardERC1155.address
  );
  const [nftShard1155Contract, setNftShard1155Contract] = useState();

  React.useEffect(() => {
    batch(async () => {
      localStorage.setItem(ITEMS_NAME.SHARD_CONTRACT_ADDRESS, address);
      await dispatch(openLoading());
      await fetchData();
      await dispatch(hideLoading());
    });
  }, [nftShard1155Contract]);

  async function fetchData(currentPage) {
    if (!nftShard1155Contract) return;
    try {
      setDataList((prev) => ({ ...prev, loading: true }));
      const total = await nftShard1155Contract.methods.numberOfToken().call();
      if (+total < 1) {
        setDataList({ ...initDataList, total, loading: false });
      } else {
        await fetchList(currentPage ?? 1, dataList.pageSize, +total);
      }
    } catch (e) {
      console.error(e);
      setDataList({ ...initDataList, loading: false });
    }
  }

  async function fetchList(currentPage, pageSize, total) {
    if (!nftShard1155Contract) return;
    try {
      setDataList((prev) => ({ ...prev, total, loading: true }));
      const offset = (currentPage - 1) * pageSize;
      const rs = await nftShard1155Contract.methods
        .tokenByIndexBatch(offset, pageSize)
        .call();

      const tokenList = parseData(rs);
      setDataList((prev) => ({
        ...prev,
        dataSource: tokenList,
        currentPage: currentPage ?? 1,
        loading: false,
        errors: [],
      }));
    } catch (e) {
      console.error(e);
      setDataList({ ...initDataList, loading: false });
    }
  }

  function parseData(rawData) {
    const data = rawData?.map((item) => {
      if (+item.totalSupply > 0) {
        return { id: item.id, total: item.totalSupply, key: item.id };
      }
    });
    return data;
  }

  const { currentPage, total, pageSize, dataSource, loading } = dataList;
  const columns = [
    {
      title: "tokenId",
      dataIndex: "id",
    },
    {
      title: "totalSuply",
      dataIndex: "total",
    },
    {
      title: "Action",
      dataIndex: "tokenId",
      render: (_, item) => (
        <Button onClick={() => navigate(`/shard/${item.id}`)}>
          {t("details")}
        </Button>
      ),
    },
  ];

  const handlePageChange = (page) => {
    fetchData(page);
  };

  return (
    <div className="egg_container">
      <Spin spinning={loading || false} size="medium">
        <ContractInfomation
          address={address}
          abi={nftShardERC1155.abi}
          setContract={setNftShard1155Contract}
          total={total}
          setAddress={setAddress}
        />
        <Table
          size="small"
          className={style.table}
          columns={columns}
          dataSource={dataSource}
          pagination={{
            defaultCurrent: 1,
            defaultPageSize: pageSize,
            position: ["bottomCenter"],
            showSizeChanger: false,
            total: dataSource?.length > 0 && total ? total : 0,
            onChange: handlePageChange,
            current: currentPage,
            hideOnSinglePage: true,
            responsive: true,
            showLessItems: true,
            className: style.paginator_custom,
          }}
          scroll={{ x: 638 }}
        />
      </Spin>
    </div>
  );
}

export default Shard;
