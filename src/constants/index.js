export const ITEMS_NAME = {
  MATRIX_CONTRACT_ADDRESS: "matrix_contract_address_v2.1",
  SHARD_CONTRACT_ADDRESS: "shard_contract_address_v2.1",
  ARCANA_CONTRACT_ADDRESS: "arcana_contract_address_v2.1",
  EGG_CONTRACT_ADDRESS: "egg_contract_address_v2.1",
}
