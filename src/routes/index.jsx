import { Spin } from "antd";
import ShardDetail from "features/shard/ShardDetail";
import React, { Suspense } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import NotFound from "../components/NotFound/NotFound";

const App = React.lazy(() => import("../components/App/App"));
const Layout = React.lazy(() => import("../components/Layout/Layout"));
const Arcana = React.lazy(() => import("../features/arcana"));
const Egg = React.lazy(() => import("../features/egg"));
const Shard = React.lazy(() => import("../features/shard"));
const Matrix = React.lazy(() => import("../features/matrix"));

function LoadingPage() {
  // return <Spin className="position-center" size="large" />;
  return <React.Fragment />;
}

export default function Router() {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <Suspense fallback={<LoadingPage />}>
            <App />
          </Suspense>
        }
      >
        <Route
          path="/"
          element={
            <Suspense fallback={<LoadingPage />}>
              <Layout />
            </Suspense>
          }
        >
          <Route
            path="/"
            element={
              <Suspense fallback={<LoadingPage />}>
                <Navigate to="/arcana" />
              </Suspense>
            }
          />
          <Route
            path="/arcana"
            element={
              <Suspense fallback={<LoadingPage />}>
                <Arcana />
              </Suspense>
            }
          />
          <Route
            path="/egg"
            element={
              <Suspense fallback={<LoadingPage />}>
                <Egg />
              </Suspense>
            }
          />
          <Route
            path="/shard"
            element={
              <Suspense fallback={<LoadingPage />}>
                <Shard />
              </Suspense>
            }
          />
          <Route
            path="/shard/:tokenId"
            element={
              <Suspense fallback={<LoadingPage />}>
                <ShardDetail />
              </Suspense>
            }
          />
          <Route
            path="/matrix"
            element={
              <Suspense fallback={<LoadingPage />}>
                <Matrix />
              </Suspense>
            }
          />
          <Route
            path="/404"
            element={
              <Suspense fallback={<LoadingPage />}>
                <NotFound />
              </Suspense>
            }
          />
          <Route
            path="*"
            element={
              <Suspense fallback={<LoadingPage />}>
                <Navigate to="404" />
              </Suspense>
            }
          />
        </Route>
      </Route>
    </Routes>
  );
}
