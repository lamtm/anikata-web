import { RPC_URL } from "configs";
import Web3 from "web3";
// import { ethers } from "ethers";

export const web3 = new Web3(new Web3.providers.HttpProvider(RPC_URL));

export const getContract = (abi, address) => {
  // const provider = new ethers.providers.Web3Provider(ANICANA_RINKEBY);
  // const signer = provider.getSigner();

  return {
    // ethereumContract: new ethers.Contract(address, abi, signer),
    web3Contract: new web3.eth.Contract(abi, address, {}),
  };
};
