import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import RootLoading from "./components/RootLoading";
import "./index.scss";
import Router from "./routes";
import { store } from "./store";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  // <React.StrictMode>
  <Provider store={store}>
    <RootLoading />
    <BrowserRouter>
      <Router />
    </BrowserRouter>
  </Provider>
  // </React.StrictMode>
);
