import arcanaERC721Abi from "./abi/ArcanaERC721.json";
import eggERC721Abi from "./abi/EggERC721.json";
import shardERC1155Abi from "./abi/ShardERC1155.json";
import matrixAbi from "./abi/Matrix.json";

export const nftArcanaERC721 = {
  address: process.env.REACT_APP_ARCANA_ERC721_ADDRESS,
  abi: arcanaERC721Abi,
};

export const nftEggERC721 = {
  address: process.env.REACT_APP_EGG_ERC721_ADDRESS,
  abi: eggERC721Abi,
};

export const nftShardERC1155 = {
  address: process.env.REACT_APP_SHARD_ERC1155_ADDRESS,
  abi: shardERC1155Abi,
};

export const nftMatrix = {
  address: process.env.REACT_APP_MATRIX_ADDRESS,
  abi: matrixAbi,
};

export const ETHERSCAN_DOMAIN = process.env.REACT_APP_ETHERSCAN_DOMAIN;
export const IPFS_DOMAIN = process.env.REACT_APP_IPFS_DOMAIN;
export const RPC_URL = process.env.REACT_APP_RPC_URL;

/ Moralis init code /;
export const serverUrl = "https://c6xzrwo6urea.usemoralis.com:2053/server";
export const appId = "sWesqVcL11oZimrXPx1hKUdRz10w0rs4r8Yioqn7";
export const masterKey = "j4577pTytWDw0oeOdN7GRz4kH6Ylr8Em6gazMIlG";
