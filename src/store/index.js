import { configureStore } from "@reduxjs/toolkit";
import AppReducer from "../components/App/AppSlice";

export const store = configureStore({
  reducer: {
    app: AppReducer,
  },
  devTools: process.env.NODE_ENV !== "production",
});
